package com.afpa.cda.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonneDto {
	private int id;
	private String nom;
	private String prenom;
	private Date naissance;
	private String metier;
	private String adresse;
	
	public PersonneDto() {
	}

	public PersonneDto(int id, String nom, String prenom, Date naissance, String metier, String adresse) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.naissance = naissance;
		this.metier = metier;
		this.adresse = adresse;
	}

}
