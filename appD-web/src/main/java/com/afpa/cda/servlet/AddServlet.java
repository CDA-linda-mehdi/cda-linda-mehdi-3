package com.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IPersonneService;
import com.afpa.cda.tools.Utils;

@WebServlet(urlPatterns = { "/add.do" })
public class AddServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		writer.append("<html>");
		writer.append("<head>");
		writer.append("<link href=\"fontawesome/css/all.min.css\" rel=\"stylesheet\">");
		writer.append("<link href=\"bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">");
		writer.append("</head>");
		writer.append("<body>");

		writer.append("<div class=\"container my-4\">\r\n" + "  <div class=\"row justify-content-md-center\">\r\n"
				+ "    <div class=\"col col-lg-8\">");

		writer.append("<form action=\"add.do\" method=\"post\" class=\"form-example\">");

		writer.append("<div class=\"form-example\">");
		writer.append("<label for=\"nom\">Nom : </label>");
		writer.append("<input type=\"text\" name=\"nom\" id=\"nom\">");
		writer.append("</div>");

		writer.append("<div class=\"form-example\">");
		writer.append("<label for=\"prenom\">Pr�nom : </label>");
		writer.append("<input type=\"text\" name=\"prenom\" id=\"prenom\">");
		writer.append("</div>");

		writer.append("<div class=\"form-example\">");
		writer.append("<input type=\"submit\" value=\"Ajouter l'utilisateur\">");
		writer.append("</div>");

		writer.append("</form>");

		writer.append("<a class=\"btn btn-secondary\" href=\"list.do\" role=\"button\">retour vers la liste</a>");
		writer.append("</div>\r\n" + "  </div>\r\n" + "</div>");

		writer.append("<script src=\"jquery/jquery-3.3.1.slim.min.js\" ></script>");
		writer.append("<script src=\"bootstrap/js/bootstrap.bundle.min.js\" ></script>");

		writer.append("</body>");
		writer.append("</html>");

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nom = req.getParameter("nom");
		String prenom = req.getParameter("prenom");

		String msg = "";
		ReponseStatut statut = ReponseStatut.OK;
		
		try {
		
		if (nom == null || nom.length() == 0 || prenom == null || prenom.length() == 0) {
			statut = ReponseStatut.KO;
			msg = "le nom et le pr�nom sont obligatoires";
		} else {

			PersonneDto pers = PersonneDto.builder().nom(nom).prenom(prenom).build();

			if (this.personneService.add(pers)) {
				msg = "ajout reussie avec succes";
			}
		}

		} catch (Exception e) {
			statut = ReponseStatut.KO;
			msg = "ajout reussie avec succes";
		}

		req.setAttribute("reponse", ReponseDto.builder().status(statut).msg(msg).build());
		
		resp.sendRedirect("index.html");
		// super.doPost(req, resp);

	}
}
