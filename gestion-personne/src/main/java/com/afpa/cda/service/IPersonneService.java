package com.afpa.cda.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.afpa.cda.dto.PersonneDto;

public interface IPersonneService {
	public List<PersonneDto> chercherToutesLesPersonnes();
	public PersonneDto chercherUnePersonnes(@Param("param")Integer id);
	public List<PersonneDto> supprimerUnePersonne(@Param("param")Integer id);
}
