package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.PersonneRepository;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.entity.Personne;

@Service
public class PersonneServiceImpl implements IPersonneService {

	@Autowired
	private PersonneRepository personneRepository;

	@Override
	public List<PersonneDto> chercherToutesLesPersonnes() {
		return this.personneRepository.findAll().stream()
				.map(e -> new PersonneDto(e.getId(), e.getName(), e.getPrenom())).collect(Collectors.toList());
	}

	@Override
	public PersonneDto chercherUnePersonnes(Integer id) {
		PersonneDto pDto;
		Optional<Personne> p = personneRepository.findById(id);
		if (p.isPresent()) {
			pDto = new PersonneDto(p.get().getId(), p.get().getName(), p.get().getPrenom(), p.get().getDateNaissance(),
					p.get().getMetier(), p.get().getAdresse());
		} else {
			pDto = new PersonneDto();
		}
		return pDto;
	}

	@Override
	public List<PersonneDto> supprimerUnePersonne(Integer id) {
		List<Personne> listPers = personneRepository.findAll();
		for (Personne personne : listPers) {
			if (personne.getId() == id) {
				
			}
		}
		return null;
	}

}
