package com.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.service.IPersonneService;

/**
 * Servlet implementation class Accueil
 */
@WebServlet(urlPatterns = { "/show.html" })
public class Show extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		String idStr = req.getParameter("param");
		Integer id = Integer.parseInt(idStr);
		PrintWriter writer = resp.getWriter();
		writer.append("<html>");
		writer.append("<head>");
		writer.append("<link rel=\"stylesheet\" href=\"tableau.css\">");
		writer.append("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">");
		writer.append("</head>");
		writer.append("<body>");
		writer.append("<table>");
		writer.append("<thead>");
		writer.append("<tr>");
		writer.append("<th>ID</th>");
		writer.append("<th>Nom</th>");
		writer.append("<th>Prénom</th>");
		writer.append("<th>Date de naissance</th>");
		writer.append("<th>Adresse</th>");
		writer.append("<th>Métier</th>");
		writer.append("</tr>");
		writer.append("</thead>");

		writer.append("<tbody>");
		PersonneDto p = this.personneService.chercherUnePersonnes(id);
		writer.append("<tr>");
		writer.append("<td>").append(Integer.toString(p.getId())).append("</td>");
		writer.append("<td>").append(p.getNom()).append("</td>");
		writer.append("<td>").append(p.getPrenom()).append("</td>");
		writer.append("<td>").append("" + p.getDateNaissance()).append("</td>");
		writer.append("<td>").append(p.getAdresse()).append("</td>");
		writer.append("<td>").append(p.getMetier()).append("</td>");
		writer.append("</tr>");
		writer.append("</table>");
		writer.append("</body>");
		writer.append("</html>");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
